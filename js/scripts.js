$(document).ready(function(){

  $(".nav li").on("click", function() {
    $(".nav li").removeClass("active");
    $(this).addClass("active");
  });

  $(".navbar-brand").on("click", function() {
    $(".nav li").removeClass("active");
  });

  console.log("working!");

  $('.product-info').matchHeight();
  
});


var app = angular.module("grapevine", ["ngRoute", "ngAnimate"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "views/home.htm"
    })
    .when("/about", {
        templateUrl : "views/about.htm"
    })
    .when("/services", {
        templateUrl : "views/examples.htm"
    })
    .when("/equipment", {
        templateUrl : "views/examples.htm"
    })
    .when("/contact", {
        templateUrl : "views/contact.htm"
    })
});
